import 'package:flutter_test/flutter_test.dart';
import 'package:nest/repository/web_client.dart';

void main() {
  test('check login test', () async {
    WebClient webClient = WebClient();
    LoginStatus status = await webClient.checkLogin();

    expect(status, LoginStatus.authenticated);

  });
}
