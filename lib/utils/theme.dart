import 'package:flutter/material.dart';
import 'package:nest/utils/utils.dart';

class NestTheme{
  static get theme{
    return ThemeData(
        primaryColor: mainColor,
        buttonColor: mainColor,
        pageTransitionsTheme: PageTransitionsTheme()
    );
  }
}