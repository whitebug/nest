import 'package:flutter/foundation.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SharedPreferencesUtils {
  /// save variable to shared preferences
  static setVariableToShared(
      {Key key, @required String variable, @required String title}) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.setString(
        title, (variable != null && variable != '') ? variable : "");
  }

  /// get variable from shared variables
  static Future<String> getVariableFromShared(
      {Key key, @required String title}) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    String result = preferences.getString(title);
    return result;
  }
}
