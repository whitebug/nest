import 'package:flutter/foundation.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';

class WidgetUtils {
  static imageCached({Key key, String url}) async {
    return DefaultCacheManager().getSingleFile(url);
  }
}