// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

User _$UserFromJson(Map<String, dynamic> json) {
  return User(
    userId: json['userId'] as int,
    firstName: json['firstName'] as String,
    lastName: json['lastName'] as String,
    role: json['Role'] as int,
    username: json['username'] as String,
    status: json['Status'] as int,
    avatarUrl: json['avatarUrl'] as String,
    token: json['token'] as String,
  )..params = Parameters.fromJson(json['params'] as Map<String, dynamic>);
}

Map<String, dynamic> _$UserToJson(User instance) => <String, dynamic>{
      'userId': instance.userId,
      'firstName': instance.firstName,
      'lastName': instance.lastName,
      'Role': instance.role,
      'username': instance.username,
      'Status': instance.status,
      'avatarUrl': instance.avatarUrl,
      'token': instance.token,
      'params': instance.params,
    };
