import 'package:json_annotation/json_annotation.dart';
import 'package:nest/models/parameters.dart';

part 'user.g.dart';

@JsonSerializable()
class User {
  @JsonKey(nullable: false)
  int userId;
  @JsonKey(nullable: false)
  String firstName;
  @JsonKey(nullable: false)
  String lastName;
  @JsonKey(name: 'Role', nullable: false)
  int role;
  @JsonKey(nullable: false)
  String username;
  @JsonKey(name: 'Status', nullable: false)
  int status;
  @JsonKey(nullable: false)
  String avatarUrl;
  @JsonKey(nullable: false)
  String token;
  @JsonKey(name: 'params', nullable: false)
  Parameters params;

  User(
      {this.userId,
      this.firstName,
      this.lastName,
      this.role,
      this.username,
      this.status,
      this.avatarUrl,
      this.token,
      Parameters parameters});

  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);
  Map<String, dynamic> toJson() => _$UserToJson(this);

  static String get storageKey {
    return 'user';
  }
}
