import 'package:json_annotation/json_annotation.dart';

part 'user_me.g.dart';

@JsonSerializable()
class UserMe {
  @JsonKey(nullable: false)
  int userId;
  @JsonKey(name: 'ID', nullable: false)
  int id;
  @JsonKey(nullable: false)
  String firstName;
  @JsonKey(nullable: false)
  String lastName;
  @JsonKey(name: 'Sex', nullable: false)
  String sex;
  @JsonKey(nullable: false)
  String avatarUrl;
  @JsonKey(name: 'Status', nullable: false)
  int status;
  @JsonKey(nullable: false)
  int role;
  @JsonKey(nullable: false)
  bool isVkUser;
  @JsonKey(nullable: false)
  int vkId;
  @JsonKey(nullable: false)
  String email;
  @JsonKey(name: 'bdate', nullable: false)
  String birthdayDate;
  @JsonKey(nullable: false)
  String bio;
  @JsonKey(nullable: false)
  String hometown;
  @JsonKey(name: 'Password', nullable: false)
  String password;

  UserMe({
    this.userId,
    this.id,
    this.firstName,
    this.lastName,
    this.sex,
    this.avatarUrl,
    this.status,
    this.role,
    this.isVkUser,
    this.vkId,
    this.email,
    this.birthdayDate,
    this.bio,
    this.hometown,
    this.password,
  });

  factory UserMe.fromJson(Map<String, dynamic> json) => _$UserMeFromJson(json);
  Map<String, dynamic> toJson() => _$UserMeToJson(this);

  static String get storageKey {
    return 'user_me';
  }
}
