// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'parameters.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Parameters _$ParametersFromJson(Map<String, dynamic> json) {
  return Parameters(
    promotedBy: json['promotedBy'] as int,
    attributes: json['attributes'] as String,
    email: json['email'] as String,
    bio: json['bio'] as String,
    birthdayDate: json['bdate'] as String,
    hometown: json['hometown'] as String,
  );
}

Map<String, dynamic> _$ParametersToJson(Parameters instance) =>
    <String, dynamic>{
      'promotedBy': instance.promotedBy,
      'attributes': instance.attributes,
      'email': instance.email,
      'bio': instance.bio,
      'bdate': instance.birthdayDate,
      'hometown': instance.hometown,
    };
