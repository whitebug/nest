import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nest/blocs/blocs.dart';
import 'package:nest/utils/utils.dart';
import 'package:nest/widgets/nest_drawer.dart';

class HomeScreen extends StatefulWidget {
  static const routeName = '/';
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {

  LoginBloc _loginBloc;

  @override
  void initState() {
    super.initState();
    _loginBloc = BlocProvider.of<LoginBloc>(context);
    _loginBloc.dispatch(CheckLoginEvent());
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Гнездышко'),
      ),
      drawer: NestDrawer(),
      body: Container(
        child: Center(
          child: Text('Для логина нажмите на drawer'),
        ),
      ),
      resizeToAvoidBottomInset: false
    );
  }
}
