import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nest/blocs/blocs.dart';
import 'package:nest/screens/screens.dart';
import 'package:nest/utils/utils.dart';
import 'package:nest/widgets/nest_drawer.dart';

class PersonalScreen extends StatefulWidget {
  static const routeName = '/personal';
  _PersonalScreenState createState() => _PersonalScreenState();
}

class _PersonalScreenState extends State<PersonalScreen> {
  final _firstNameController = TextEditingController();
  final _lastNameController = TextEditingController();
  PersonalBloc _personalBloc;

  @override
  void initState() {
    super.initState();
    _personalBloc = BlocProvider.of<PersonalBloc>(context);
    _personalBloc.dispatch(ShowPersonalEvent());
  }

  @override
  Widget build(BuildContext context) {

    final _loginBloc = BlocProvider.of<LoginBloc>(context);
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Личный кабинет'),
      ),
      drawer: NestDrawer(),
      body: BlocBuilder(
        bloc: _loginBloc,
        builder: (BuildContext context, LoginState state) {
          if (state is LogInState) {
            return Container(
              child: SingleChildScrollView(
                child: Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Card(
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 15.0, vertical: 5.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              FlatButton(
                                child: Text('Выйти'),
                                color: Colors.grey[100],
                                onPressed: () {
                                  _loginBloc.dispatch(LogOutEvent());
                                  Navigator.pushReplacementNamed(context, LoginScreen.routeName);
                                },
                              )
                            ],
                          ),
                        ),
                      ),
                      Card(
                        child: Center(
                          child: Padding(
                            padding: const EdgeInsets.all(15.0),
                            child: AutoSizeText(
                              'Личный кабинет',
                              maxLines: 2,
                              minFontSize: 4,
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                fontSize: SizeConfig.safeBlockAverage * 4,
                              ),
                            ),
                          ),
                        ),
                      ),
                      BlocBuilder(
                        bloc: _personalBloc,
                        builder: (BuildContext context, PersonalState state) {
                          if (state is InfoPersonalState) {
                            _firstNameController.text = state.userMe.firstName;
                            _lastNameController.text = state.userMe.lastName;
                            return Card(
                              child: Padding(
                                padding: const EdgeInsets.all(15.0),
                                child: Column(
                                  mainAxisSize: MainAxisSize.min,
                                  children: <Widget>[
                                    FutureBuilder(
                                      future: WidgetUtils.imageCached(url: state.userMe.avatarUrl),
                                      builder: (BuildContext context, AsyncSnapshot snapshot) {
                                        if(snapshot.hasData){
                                          return Image.file(
                                            snapshot.data,
                                          );
                                        } else {
                                          return Image.asset(
                                            'assets/logo/nestLogo.jpg',
                                          );
                                        }
                                      },
                                    ),
                                    TextField(
                                      controller: _firstNameController,
                                      keyboardType: TextInputType.emailAddress,
                                      decoration: InputDecoration(labelText: 'Имя'),
                                    ),
                                    TextField(
                                      controller: _lastNameController,
                                      keyboardType: TextInputType.visiblePassword,
                                      decoration: InputDecoration(labelText: 'Фамилия'),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(top: 15.0),
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.end,
                                        children: <Widget>[
                                          RaisedButton(
                                            child: Text(
                                              'Обновить данные',
                                              style: TextStyle(
                                                color: Colors.white,
                                              ),
                                            ),
                                            onPressed: () {
                                              _personalBloc.dispatch(UpdatePersonalEvent(
                                                firstName: _firstNameController.text,
                                                lastName: _lastNameController.text,
                                                avatar: state.userMe.avatarUrl,
                                              ));
                                            },
                                          ),
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            );
                          } else if (state is ErrorPersonalState) {
                            _firstNameController.text = '';
                            _lastNameController.text = '';
                            return Container(
                              height: SizeConfig.safeBlockVertical * 100,
                              child: Center(
                                child: Column(
                                  mainAxisSize: MainAxisSize.min,
                                  children: <Widget>[
                                    Text(
                                      state.error,
                                    ),
                                    RaisedButton(
                                      child: Text(
                                        'Назад',
                                        style: TextStyle(
                                          color: Colors.white,
                                        ),
                                      ),
                                      onPressed: () {
                                        Navigator.pushReplacementNamed(context, LoginScreen.routeName);
                                      },
                                    )
                                  ],
                                ),
                              ),
                            );
                          } else {
                            _firstNameController.text = '';
                            _lastNameController.text = '';
                            return Container(
                              child: Center(
                                child: CircularProgressIndicator(),
                              ),
                            );
                          }
                        },
                      ),
                    ],
                  ),
                ),
              ),
            );
          } else if (state is LogOutState) {
            _firstNameController.text = '';
            _lastNameController.text = '';
            return Container(
              child: Center(
                child: CircularProgressIndicator(),
              ),
            );
          } else if (state is ErrorLoginState) {
            _firstNameController.text = '';
            _lastNameController.text = '';
            return Container(
              height: SizeConfig.safeBlockVertical * 100,
              child: Center(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Text(
                      state.error,
                    ),
                    RaisedButton(
                      child: Text(
                        'Назад',
                        style: TextStyle(
                          color: Colors.white,
                        ),
                      ),
                      onPressed: () {
                        Navigator.pushReplacementNamed(context, LoginScreen.routeName);
                      },
                    )
                  ],
                ),
              ),
            );
          } else {
            return Container(
              child: Center(
                child: CircularProgressIndicator(),
              ),
            );
          }
        },
      ),
    );
  }
}
