import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nest/blocs/blocs.dart';
import 'package:nest/screens/register_screen.dart';
import 'package:nest/screens/screens.dart';
import 'package:nest/utils/size_config.dart';
import 'package:nest/widgets/nest_drawer.dart';

class LoginScreen extends StatelessWidget {
  static const routeName = '/login';
  @override
  Widget build(BuildContext context) {
    final _loginBloc = BlocProvider.of<LoginBloc>(context);
    final _loginController = TextEditingController();
    final _passwordController = TextEditingController();
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Войти'),
      ),
      drawer: NestDrawer(),
      body: Container(
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Card(
                  child: Center(
                    child: Padding(
                      padding: const EdgeInsets.all(15.0),
                      child: AutoSizeText(
                        'Введите ваш логин и пароль',
                        maxLines: 2,
                        minFontSize: 4,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontSize: SizeConfig.safeBlockAverage * 4,
                        ),
                      ),
                    ),
                  ),
                ),
                Card(
                  child: Padding(
                    padding: const EdgeInsets.all(15.0),
                    child: Column(
                      children: <Widget>[
                        TextField(
                          controller: _loginController,
                          keyboardType: TextInputType.emailAddress,
                          decoration: InputDecoration(labelText: 'email'),
                        ),
                        TextField(
                          controller: _passwordController,
                          keyboardType: TextInputType.visiblePassword,
                          decoration: InputDecoration(labelText: 'пароль'),
                          obscureText: true,
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 15.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              FlatButton(
                                color: Colors.grey[100],
                                child: AutoSizeText(
                                  'Зарегистрироваться',
                                  minFontSize: 4,
                                ),
                                onPressed: () {
                                  Navigator.pushReplacementNamed(context, RegisterScreen.routeName);
                                },
                              ),
                              RaisedButton(
                                child: AutoSizeText(
                                  'Войти',
                                  minFontSize: 4,
                                  style: TextStyle(
                                    color: Colors.white,
                                  ),
                                ),
                                onPressed: () {
                                  _loginBloc.dispatch(EnterLoginEvent(
                                    login: _loginController.text,
                                    password: _passwordController.text,
                                  ));
                                  Navigator.pushReplacementNamed(context, PersonalScreen.routeName);
                                },
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
