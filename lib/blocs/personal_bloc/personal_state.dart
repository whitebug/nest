import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:nest/models/models.dart';

abstract class PersonalState extends Equatable {
  const PersonalState();
}

class NoInfoPersonalState extends PersonalState {
  @override
  List<Object> get props => [];

  @override
  String toString() => 'NoPersonalState';
}

class InfoPersonalState extends PersonalState {
  final UserMe userMe;

  InfoPersonalState({@required this.userMe});

  @override
  List<Object> get props => [];

  @override
  String toString() =>
      'InfoPersonalState {user.lastName: ${userMe.lastName} user.firstName: ${userMe.firstName}}';
}

class ErrorPersonalState extends PersonalState {
  final String error;

  ErrorPersonalState({@required this.error});

  @override
  List<Object> get props => [];

  @override
  String toString() => 'ErrorPersonalState {error: $error';
}
