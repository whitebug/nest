import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class PersonalEvent extends Equatable {
  const PersonalEvent();
}

class LoadingPersonalEvent extends PersonalEvent {
  @override
  List<Object> get props => null;

  @override
  String toString() => 'LoadingPersonalEvent';
}

class ShowPersonalEvent extends PersonalEvent {
  @override
  List<Object> get props => null;

  @override
  String toString() => 'ShowPersonalEvent';
}

class UpdatePersonalEvent extends PersonalEvent {
  final String firstName;
  final String lastName;
  final String avatar;

  UpdatePersonalEvent({@required this.firstName, @required this.lastName, this.avatar});

  @override
  List<Object> get props => null;

  @override
  String toString() => 'UpdatePersonalEvent';
}
