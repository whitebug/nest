import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:nest/blocs/blocs.dart';
import 'package:nest/models/models.dart';
import 'package:nest/repository/repository.dart';
import './personal.dart';

class PersonalBloc extends Bloc<PersonalEvent, PersonalState> {
  final NestRepository nestRepository;
  final LoginBloc loginBloc;
  StreamSubscription stoGroupSubscription;

  PersonalBloc({@required this.loginBloc, @required this.nestRepository}) {
    stoGroupSubscription = loginBloc.state.listen((state) {
      if (state is LogInState) {
        dispatch(LoadingPersonalEvent());
        dispatch(ShowPersonalEvent());
      }
    });
  }

  @override
  void dispose() {
    stoGroupSubscription.cancel();
    super.dispose();
  }

  @override
  PersonalState get initialState => NoInfoPersonalState();

  @override
  Stream<PersonalState> mapEventToState(
    PersonalEvent event,
  ) async* {
    if (event is LoadingPersonalEvent) {
      yield* _mapLoadingPersonalEventToState();
    } else if (event is ShowPersonalEvent) {
      yield* _mapShowPersonalEventToState();
    } else if (event is UpdatePersonalEvent) {
      yield* _mapUpdatePersonalEventToState(event);
    }
  }

  Stream<PersonalState> _mapLoadingPersonalEventToState() async* {
    yield NoInfoPersonalState();
  }

  Stream<PersonalState> _mapShowPersonalEventToState() async* {
    try {
      UserMe userMe = await nestRepository.getUserInfo();
      yield InfoPersonalState(userMe: userMe);
    } catch (e) {
      print(e);
      ErrorPersonalState(error: e);
    }
  }

  Stream<PersonalState> _mapUpdatePersonalEventToState(
      UpdatePersonalEvent event) async* {
    try {
      await nestRepository.updateUserInfo(
          firstName: event.firstName,
          lastName: event.lastName,
          avatar: event.avatar);
      yield NoInfoPersonalState();
      UserMe userMe = await nestRepository.getUserInfo();
      yield InfoPersonalState(userMe: userMe);
    } catch (e) {
      print(e);
      ErrorPersonalState(error: e);
    }
  }
}
