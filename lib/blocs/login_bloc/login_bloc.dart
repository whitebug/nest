import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:nest/blocs/blocs.dart';
import 'package:nest/models/models.dart';
import 'package:nest/repository/repository.dart';
import 'package:nest/utils/utils.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  final NestRepository nestRepository;

  LoginBloc({@required this.nestRepository});

  @override
  LoginState get initialState => WaitingForLoginState();

  @override
  Stream<LoginState> mapEventToState(
    LoginEvent event,
  ) async* {
    if (event is CheckLoginEvent) {
      yield* _mapCheckLoginEventToState();
    } else if (event is EnterLoginEvent) {
      yield* _mapEnterLoginEventToState(event);
    } else if (event is RegisterLoginEvent) {
      yield* _mapRegisterLoginEventToState(event);
    } else if (event is LogOutEvent) {
      yield* _mapLogOutEventToState();
    }
  }

  Stream<LoginState> _mapCheckLoginEventToState() async* {
    try {
      String login =
          await SharedPreferencesUtils.getVariableFromShared(title: 'Login');
      String password =
          await SharedPreferencesUtils.getVariableFromShared(title: 'Password');
      User user = await nestRepository.login(login: login, password: password);
      yield LogInState(user: user);
    } catch (e) {
      print(e);
    }
  }

  Stream<LoginState> _mapEnterLoginEventToState(EnterLoginEvent event) async* {
    try {
      User user = await nestRepository.login(
          login: event.login, password: event.password);
      yield LogInState(user: user);
    } catch (e) {
      print(e);
      yield ErrorLoginState(error: e.toString());
    }
  }

  Stream<LoginState> _mapRegisterLoginEventToState(
      RegisterLoginEvent event) async* {
    try {
      User user = await nestRepository.register(
          login: event.login, password: event.password);
      yield LogInState(user: user);
    } catch (e) {
      print(e);
    }
  }

  Stream<LoginState> _mapLogOutEventToState() async* {
    try {
      await nestRepository.logout();
      yield LogOutState();
    } catch (e) {
      print(e);
    }
  }
}
