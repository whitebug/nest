import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'package:nest/models/models.dart';
import 'package:nest/utils/utils.dart';

enum LoginStatus { unauthenticated, authenticated }

class WebClient {
  const WebClient();

  final String url = 'https://m.ugnest.com/rpc/v1';

  Future<LoginStatus> checkLogin() async {
    String login =
        await SharedPreferencesUtils.getVariableFromShared(title: 'Login');
    var body = json.encode({
      "id": "5a9c5e40-37b0-4de5-9b12-69804f4bdd19",
      "jsonrpc": "2.0",
      "method": "auth.CheckLogin",
      "params": {"login": "$login"}
    });

    final response = await http.post('$url',
        headers: {'Content-Type': 'application/json'}, body: body);

    if (response.statusCode == 200) {
      if (!response.body.contains('error')) {
        final parsed = json.decode(response.body);
        var result = parsed['result'];
        if (result) {
          return LoginStatus.authenticated;
        } else {
          return LoginStatus.unauthenticated;
        }
      } else {
        return LoginStatus.unauthenticated;
      }
    } else {
      return LoginStatus.unauthenticated;
    }
  }

  Future<User> register(
      {@required String login, @required String password}) async {
    var body = json.encode({
      "id": "368171e7-a95e-444b-a029-f5fe0e564639",
      "jsonrpc": "2.0",
      "method": "auth.Reg",
      "params": {"phone": "$login", "password": "$password", "sessionId": ""}
    });

    final response = await http.post('$url',
        headers: {'Content-Type': 'application/json'}, body: body);

    if (response.statusCode == 200) {
      final parsed = json.decode(response.body);
      if (response.body.contains('result') && parsed['result'] != null) {
        User user = User.fromJson(parsed['result']);
        SharedPreferencesUtils.setVariableToShared(
            title: 'Login', variable: login);
        SharedPreferencesUtils.setVariableToShared(
            title: 'Password', variable: password);
        SharedPreferencesUtils.setVariableToShared(
            title: 'Token', variable: user.token);
        return user;
      } else {
        switch (parsed['error']['code']) {
          case '507':
            throw ('Логин занят');
          default:
            throw ('Ошибка сети');
        }
      }
    } else {
      throw ('Ошибка сети');
    }
  }

  Future<User> login(
      {@required String login, @required String password}) async {
    var body = json.encode({
      "id": "89863c4a-ee6c-4ed3-bbf9-ad0143f1e72c",
      "jsonrpc": "2.0",
      "method": "auth.Login",
      "params": {"phone": "$login", "password": "$password", "sessionId": ""}
    });

    final response = await http.post('$url',
        headers: {'Content-Type': 'application/json'}, body: body);

    final parsed = json.decode(response.body);
    if (response.statusCode == 200) {
      if (response.body.contains('result') && parsed['result'] != null) {
        User user = User.fromJson(parsed['result']);
        SharedPreferencesUtils.setVariableToShared(
            title: 'Login', variable: login);
        SharedPreferencesUtils.setVariableToShared(
            title: 'Password', variable: password);
        SharedPreferencesUtils.setVariableToShared(
            title: 'Token', variable: user.token);
        return user;
      } else {
        throw (parsed['error']['message']);
      }
    } else {
      throw ('Ошибка сети');
    }
  }

  Future<UserMe> getUserInfo() async {
    var body = json.encode({
      "id": "63975845-8d29-4147-96bf-ca2078d84655",
      "jsonrpc": "2.0",
      "method": "users.Me",
      "params": {}
    });

    String token =
        await SharedPreferencesUtils.getVariableFromShared(title: 'Token');

    final response = await http.post('$url',
        headers: {
          HttpHeaders.authorizationHeader: "Bearer $token",
          'Content-Type': 'application/json'
        },
        body: body);

    if (response.statusCode == 200) {
      final parsed = json.decode(response.body);
      return UserMe.fromJson(parsed['result']);
    } else {
      throw ('Ошибка сети');
    }
  }

  Future updateUserInfo(
      {@required String firstName,
      @required String lastName,
      String avatar}) async {
    var body = json.encode({
      "id": "289ae50a-bd99-43f7-9d40-28cd56d68c47",
      "jsonrpc": "2.0",
      "method": "users.Update",
      "params": {
        "user": {
          "lastName": "$lastName",
          "firstName": "$firstName",
          "avatarUrl": "$avatar"
        }
      }
    });

    String token =
        await SharedPreferencesUtils.getVariableFromShared(title: 'Token');

    final response = await http.post('$url',
        headers: {
          HttpHeaders.authorizationHeader: "Bearer $token",
          'Content-Type': 'application/json'
        },
        body: body);

    if (response.statusCode != 200) {
      throw ('Ошибка сети');
    }
  }
}
