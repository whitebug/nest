import 'package:meta/meta.dart';
import 'package:nest/models/models.dart';
import 'package:nest/repository/repository.dart';
import 'package:nest/utils/utils.dart';

class NestRepository {
  final WebClient webClient;

  NestRepository({
    this.webClient = const WebClient(),
  });

  Future<LoginStatus> checkLogin() async {
    return await webClient.checkLogin();
  }

  Future<User> register(
      {@required String login, @required String password}) async {
    return await webClient.register(login: login, password: password);
  }

  Future<User> login(
      {@required String login, @required String password}) async {
    return await webClient.login(login: login, password: password);
  }

  Future logout() async {
    SharedPreferencesUtils.setVariableToShared(title: 'Login', variable: '');
    SharedPreferencesUtils.setVariableToShared(title: 'Password', variable: '');
    SharedPreferencesUtils.setVariableToShared(title: 'Token', variable: '');
  }

  Future<UserMe> getUserInfo() async {
    return await webClient.getUserInfo();
  }

  Future updateUserInfo(
      {@required String firstName, @required String lastName, String avatar}) async {
    return await webClient.updateUserInfo(
        firstName: firstName, lastName: lastName, avatar: avatar);
  }
}
