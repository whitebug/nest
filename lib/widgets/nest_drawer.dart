import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nest/blocs/blocs.dart';
import 'package:nest/screens/screens.dart';

class NestDrawer extends StatelessWidget {
  Widget buildListTile(String title, IconData icon, Function tapHandler) {
    return ListTile(
      leading: Icon(
        icon,
        size: 26,
      ),
      title: Text(
        title,
        style: TextStyle(
          fontSize: 20,
        ),
      ),
      onTap: tapHandler,
    );
  }

  @override
  Widget build(BuildContext context) {
    final LoginBloc _loginBloc = BlocProvider.of<LoginBloc>(context);
    return Drawer(
      child: BlocBuilder(
        bloc: _loginBloc,
        builder: (BuildContext context, LoginState state){
          return Column(
            children: <Widget>[
              Container(
                width: double.infinity,
                padding: EdgeInsets.only(left: 30.0, top: 40.0, right: 30.0, bottom: 10.0),
                alignment: Alignment.centerLeft,
                child: Image.asset(
                  'assets/logo/nestLogo.jpg',
                  fit: BoxFit.cover,
                ),
              ),
              Divider(),
              buildListTile('Домой', Icons.home, () {
                Navigator.of(context).pop();
                Navigator.pushReplacementNamed(context, HomeScreen.routeName);
              }),
              state is LogInState
              ? buildListTile('Личный кабинет', Icons.person, () {
                  Navigator.of(context).pop();
                  Navigator.pushReplacementNamed(context, PersonalScreen.routeName);
                })
              : buildListTile('Войти', Icons.lock_open, () {
                  Navigator.of(context).pop();
                  Navigator.pushReplacementNamed(context, LoginScreen.routeName);
                }),
            ],
          );
        },
      ),
    );
  }
}
