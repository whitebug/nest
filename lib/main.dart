import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nest/repository/repository.dart';
import 'package:nest/blocs/bloc_delegate.dart';
import 'package:nest/blocs/blocs.dart';
import 'package:nest/repository/nest_repository.dart';
import 'package:nest/screens/home_screen.dart';
import 'package:nest/utils/theme.dart';
import 'package:nest/utils/utils.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  BlocSupervisor.delegate = StoBlocDelegate();
  runApp(NestApp());
}

class NestApp extends StatelessWidget {

  final _loginBloc = LoginBloc(
    nestRepository: NestRepository(),
  );

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: <BlocProvider>[
        BlocProvider<LoginBloc>(
          builder: (BuildContext context) => _loginBloc,
        ),
        BlocProvider<PersonalBloc>(
          builder: (BuildContext context) => PersonalBloc(
            nestRepository: NestRepository(),
            loginBloc: _loginBloc,
          ),
        ),
      ],
      child: MaterialApp(
        title: 'Уютное гнездышко',
        theme: NestTheme.theme,
        onGenerateRoute: Router.generateRoute,
        initialRoute: HomeScreen.routeName,
      ),
    );
  }
}